<?php

// Definition of our plugin.
$plugin = array(
  'title' => t('Align right'),
  'description' => t('Floats element to the right side.'),
  'render pane' => 'MYMODULE_align_right_render_pane',
  'render region' => 'MYMODULE_align_right_render_region',
  'weight' => -10,
  // Additional variable added for checkboxes use.
  'class' => 'align-right',
);

/**
 * Theme function for the pane style.
 */
function theme_MYMODULE_align_right_render_pane($vars) {
  $content = $vars['content'];

  // Default class set on line 10 in this file.
  $classess = array($vars['style']['class']);
  // Add other classess added by selecting different styles.
  if (!empty($vars['classess'])) {
    $classess = array_merge ($classess, $vars['classess']);
  }
  // In case there were duplicates - remove them.
  $classess = array_unique($classess);
  // Set classess.
  $content->css_class .= ' ' . implode(' ', $classess);

  // Display the output.
  return theme('panels_pane', array('content' => $content, 'pane' => $vars['pane'], 'display' => $vars['display']));
}

/**
 * Theme function for the region style.
 */
function theme_MYMODULE_align_right_render_region($vars) {
  $display = $vars['display'];
  $region_id = $vars['region_id'];
  $panes = $vars['panes'];
  $settings = $vars['settings'];

  $output = '';
  $output .= '<div class="region region-' . $vars['region_id'] . ' align-right">';
  $output .= implode('<div class="panel-separator"></div>', $panes);
  $output .= '</div>';
  return $output;
}
